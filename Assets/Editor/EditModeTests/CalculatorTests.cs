﻿using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class CalculatorTests
    {
        [Test]
        public void Add_Success_1()
        {
            Calculator c = new Calculator();
            float a = 1f;
            float b = 2f;

            float res = c.Add(a, b);
            
            Assert.AreEqual(3,res);
        }
        
        [Test]
        public void Sub_Success_1()
        {
            Calculator c = new Calculator();
            float a = 1f;
            float b = 2f;

            float res = c.Sub(a, b);
            
            Assert.AreEqual(-1,res);
        }
        
        [Test]
        public void Div_Success_1()
        {
            Calculator c = new Calculator();
            float a = 1f;
            float b = 2f;

            float res = c.Div(a, b);
            
            Assert.AreEqual(0.5f,res);
        }
        
        [Test]
        public void Div_Fail()
        {
            Calculator c = new Calculator();
            float a = 1f;
            float b = 0f;

            Assert.Throws<ArgumentException>(() => c.Div(a, b));
        }
        
        [Test]
        public void Mul_Success_1()
        {
            Calculator c = new Calculator();
            float a = 1f;
            float b = 2f;

            float res = c.Mul(a, b);
            
            Assert.AreEqual(2,res);
        }
    }
}
